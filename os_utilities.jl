# Attribution:
# https://github.com/charlesll/Spectra.jl/blob/master/src/integrale.jl
"""
The 'trapz' function allows to perform numerical integration of a signal.
	integrale (float64) = trapz(x:vector of float 64, y:vector of float 64)
"""
function trapz{Tx<:Number, Ty<:Number}(x::Vector{Tx}, y::Vector{Ty})
    # Trapezoidal integration rule
    local n = length(x)
    if (length(y) != n)
        error("Vectors 'x', 'y' must be of same length")
    end
    r = zero(zero(Tx) + zero(Ty))
    if n == 1; return r; end
    for i in 2:n
        r += (x[i] - x[i-1]) * (y[i] + y[i-1])
    end
    trapz_int = r/2
    return trapz_int
end



# Attribution:
# http://stackoverflow.com/questions/8188548/splitting-a-js-array-into-n-arrays
function divide_into_equal_partitions(arr, n)
    if (n < 2)
        return arr
    end

    len = length(arr)
    partitions = []
    i = 1

    if (len % n == 0)
        return partition_all(arr, floor(Int, len/n))
    else
        while i <= len
            part_size = ceil(Int, (len - i + 1)/n)
            boundary = i + part_size - 1
            push!(partitions, arr[i:boundary])

            n -= 1
            i = boundary + 1
        end
        return partitions
    end
end

# Attribution:
# https://groups.google.com/forum/#!topic/julia-users/xBcQRebyi_o
function mindist(x, val)
    # using enumerate to avoid indexing
    min_i = 0
    min_x = Inf
    for (i, xi) in enumerate(x)
        # should be positive distance!
        dist = (xi - val)
        if dist < min_x && dist > 0
            min_x = dist
            min_i = i
        end
    end
    return (min_i, min_x)
end


# Attribution:
# http://stackoverflow.com/questions/26953340/partition-equivalent-in-julia
function partition_all(x, n)
    [x[i:min(i+n-1,length(x))] for i in 1:n:length(x)]
end
